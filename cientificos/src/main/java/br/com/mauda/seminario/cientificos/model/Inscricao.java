package br.com.mauda.seminario.cientificos.model;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class Inscricao {

    private Long id;
    private Boolean direitoMaterial;
    private Seminario seminario;
    private Estudante estudante;
    private SituacaoInscricaoEnum situacao = SituacaoInscricaoEnum.DISPONIVEL;

    public Inscricao(Seminario seminario) {
        this.seminario = seminario;
    }

    public void comprar(Estudante estudante, Boolean direitoMaterial) {
        if (SituacaoInscricaoEnum.DISPONIVEL.equals(this.situacao)) {
            this.situacao = SituacaoInscricaoEnum.COMPRADO;
            this.estudante = estudante;
            this.direitoMaterial = direitoMaterial;
            this.estudante.adicionarInscricao(this);
        }
    }

    public void realizarCheckIn() {
        if (SituacaoInscricaoEnum.COMPRADO.equals(this.situacao)) {
            this.situacao = SituacaoInscricaoEnum.CHECKIN;
        }
    }

    public void cancelarCompra() {
        if (SituacaoInscricaoEnum.COMPRADO.equals(this.situacao)) {
            this.situacao = SituacaoInscricaoEnum.DISPONIVEL;
            this.estudante.removerInscricao(this);
            this.estudante = null;
            this.direitoMaterial = null;
        }
    }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacao;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public Seminario getSeminario() {
        return this.seminario;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
