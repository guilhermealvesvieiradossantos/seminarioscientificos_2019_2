package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.List;

public class AreaCientifica {

    private Long id;
    private String nome;
    private List<Curso> curso = new ArrayList<>();

    public void adicionarCurso(Curso curso) {
        this.curso.add(curso);
    }

    public boolean possuiCurso(Curso curso) {
        return this.curso.contains(curso);
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Curso> getCursos() {
        return this.curso;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
